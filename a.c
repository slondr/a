const _start(){
  asm(
        "movl $len,%edx;\n"
	"movl $msg,%ecx;\n"
	"movl $1,%ebx;\n"
	"movl $4,%eax;\n"
	"int $0x80;\n"
	"movl $1,%eax;\n"
	"int $0x80;\n"
	"msg: .ascii \"Hello, world!\\n\";\n"
	"len = . - msg;\n"
   );
}
